package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SynapsKycbContractTest {

  static final String producerWebsite = "http://localhost";
  static final int producerServerJurisdiction = 123;
  static final BlockchainPublicKey producerPublicKey =
      new KeyPair(BigInteger.valueOf(123)).getPublic();
  static final BlsPublicKey producerBlsKey;
  static final BlsSignature popSignature;

  private static final KeyPair synapsKeyPair = new KeyPair(BigInteger.valueOf(4444));
  static final BlockchainPublicKey synapsPublicKey = synapsKeyPair.getPublic();

  static {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(444));
    producerBlsKey = blsKeyPair.getPublicKey();
    popSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("pop")));
  }

  static final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000123");

  private final SysContractContextTest context = new SysContractContextTest();

  private static final String KYC_NAME = "Some Individual";
  private static final String KYB_NAME = "Some Company";
  private static final String ADDRESS = "Some Street";
  private static final String CITY = "Some City";
  private static final int COUNTRY = 999;
  static final String RAW_REQUEST =
      """
      {
      %s
      "name":"%s",
      "city":"%s",
      "country":"ABC",
      "country_numeric":"%d",
      "address":"%s",
      "registration_number":"1234567890",
      "provider":"Synaps",
      "partisia_blockchain_address":"%s"}""";
  static final String KYB_REQUEST =
      RAW_REQUEST.formatted(
          "\"corporate_id\": 123,",
          KYB_NAME,
          CITY,
          COUNTRY,
          ADDRESS,
          "000070000000000000000000000000000000000001");

  static final String KYC_REQUEST =
      RAW_REQUEST.formatted(
          "", KYC_NAME, CITY, COUNTRY, ADDRESS, "000070000000000000000000000000000000000001");

  static final Signature signatureOnRawRequest =
      convertFromEthereumSignature(
          "624b6dd857f865de2d1180569bd4986af7cfb6efdf303e35c04"
              + "57109628ce7aa4b04d2c04a14f33aa101ac4ec4e7e7a5846d5844e72f19142e9b5f4d93c655eb00");

  private final SysContractSerialization<SynapsKycbContractState> serialization =
      new SysContractSerialization<>(
          SynapsKycbContractInvoker.class, SynapsKycbContractState.class);

  private SynapsKycbContractState getInitial() {
    return serialization.create(
        context,
        rpc -> {
          bpOrchestrationContract.write(rpc);
          synapsPublicKey.write(rpc);
        });
  }

  @Test
  void onCreate() {
    SynapsKycbContractState initial = getInitial();
    assertThat(initial).isNotNull();
    assertThat(initial.getBpOrchestrationContract()).isEqualTo(bpOrchestrationContract);
    assertThat(initial.getVerificationKey()).isEqualTo(synapsPublicKey);
    assertThat(initial.getKycRegistrations().size()).isEqualTo(0);
  }

  @Test
  void onUpgrade() {
    SynapsKycbContract contract = new SynapsKycbContract();
    SynapsKycbContractState newState =
        SynapsKycbContractState.initial(bpOrchestrationContract, producerPublicKey)
            .addKycRegistration(
                producerPublicKey.createAddress(),
                new KycRegistration("name", "address", "city", 12, 13, producerWebsite));
    StateAccessor stateAccessor = StateAccessor.create(newState);
    SynapsKycbContractState upgradeState = contract.upgrade(stateAccessor);
    assertThat(upgradeState).usingRecursiveComparison().isEqualTo(newState);
  }

  @Test
  void onUpgradeOldState() {
    @Immutable
    record OldState(BlockchainAddress bpOrchestrationContract, BlockchainPublicKey verificationKey)
        implements StateSerializable {}

    SynapsKycbContract contract = new SynapsKycbContract();
    OldState oldState = new OldState(bpOrchestrationContract, producerPublicKey);
    StateAccessor stateAccessor = StateAccessor.create(oldState);
    SynapsKycbContractState upgradeState = contract.upgrade(stateAccessor);
    assertThat(upgradeState)
        .usingRecursiveComparison()
        .ignoringFields("kycRegistrations")
        .isEqualTo(oldState);
    assertThat(upgradeState.getKycRegistrations().size()).isEqualTo(0);
  }

  /** Verify that approval succeeds using a key and request provided by synaps. */
  @Test
  void approve() {
    SynapsKycbContractState initial = getInitial();
    Hash message = Hash.create(s -> s.write(KYB_REQUEST.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    SynapsKycbContractState state = submitKybInvocation(initial, KYB_REQUEST, signature);
    assertThat(state).isNotNull();
    verifyBpoInteraction(KYB_REQUEST, context.getInteractions());
  }

  @Test
  void incorrectSender() {
    SynapsKycbContractState initial = getInitial();
    String kybRequestInvalidAddress =
        RAW_REQUEST.formatted(
            "\"corporate_id\": 123,",
            KYB_NAME,
            CITY,
            COUNTRY,
            ADDRESS,
            "000070000000000000000000000000000000000002");
    Hash message =
        Hash.create(s -> s.write(kybRequestInvalidAddress.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    Assertions.assertThatThrownBy(
            () -> submitKybInvocation(initial, kybRequestInvalidAddress, signature))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  void kycSubmittedToKyb() {
    SynapsKycbContractState initial = getInitial();
    Hash message = Hash.create(s -> s.write(KYC_REQUEST.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    Assertions.assertThatThrownBy(() -> submitKybInvocation(initial, KYC_REQUEST, signature))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("KYC registration submitted as KYB");
  }

  @Test
  void registerKyc() {
    SynapsKycbContractState initial = getInitial();
    SynapsKycbContractState state =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(SynapsKycbContract.Invocations.KYC_REGISTER);
              rpc.writeString(KYC_NAME);
              rpc.writeString(ADDRESS);
              rpc.writeString(CITY);
              rpc.writeInt(COUNTRY);
              rpc.writeString(producerWebsite);
              rpc.writeInt(producerServerJurisdiction);
            });
    assertThat(state).isNotNull();
    KycRegistration kycRegistration = state.getKycRegistration(context.getFrom());
    assertThat(kycRegistration).isNotNull();
  }

  @Test
  void submitKyc() {
    SynapsKycbContractState initial = withKycRegistration();
    Hash message = Hash.create(s -> s.write(KYC_REQUEST.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    SynapsKycbContractState state = submitKycInvocation(initial, KYC_REQUEST, signature);
    assertThat(state).isNotNull();
    verifyBpoInteraction(KYC_REQUEST, context.getRemoteCalls().contractEvents);
    byte[] callbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(SynapsKycbContract.kycSubmitCallback(context.getFrom()));
    assertThat(callbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void incorrectSenderKyc() {
    SynapsKycbContractState initial = withKycRegistration();
    String kycRequestWrongSender =
        RAW_REQUEST.formatted(
            "", KYC_NAME, CITY, COUNTRY, ADDRESS, "000070000000000000000000000000000000000002");
    Hash message =
        Hash.create(s -> s.write(kycRequestWrongSender.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    Assertions.assertThatThrownBy(
            () -> submitKycInvocation(initial, kycRequestWrongSender, signature))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  void inconsistentKycRegistration() {
    SynapsKycbContractState initial = withKycRegistration();
    String kycRequestWithWrongName =
        RAW_REQUEST.formatted(
            "",
            "Some other individiual",
            CITY,
            COUNTRY,
            ADDRESS,
            "000070000000000000000000000000000000000001");
    Hash message =
        Hash.create(s -> s.write(kycRequestWithWrongName.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    Assertions.assertThatThrownBy(
            () -> submitKycInvocation(initial, kycRequestWithWrongName, signature))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Information in KYC registration must match information from Synaps");
  }

  private SynapsKycbContractState withKycRegistration() {
    return getInitial()
        .addKycRegistration(
            context.getFrom(),
            new KycRegistration(
                KYC_NAME, ADDRESS, CITY, COUNTRY, producerServerJurisdiction, producerWebsite));
  }

  @Test
  void kybSubmittedToKyc() {
    SynapsKycbContractState initial = withKycRegistration();
    Hash message = Hash.create(s -> s.write(KYB_REQUEST.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(SynapsKycbContract.Invocations.KYC_SUBMIT);
                      rpc.writeString(KYB_REQUEST);
                      signature.write(rpc);
                      producerPublicKey.write(rpc);
                      producerBlsKey.write(rpc);
                      popSignature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("KYB registration submitted as KYC");
  }

  @Test
  void invalidSignature() {
    SynapsKycbContractState initial = withKycRegistration();
    Hash message = Hash.create(s -> s.write("not KYB_REQUEST".getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(SynapsKycbContract.Invocations.APPROVE);
                      rpc.writeString(KYB_REQUEST);
                      signature.write(rpc);
                      rpc.writeString(producerWebsite);
                      rpc.writeInt(producerServerJurisdiction);
                      producerPublicKey.write(rpc);
                      producerBlsKey.write(rpc);
                      popSignature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unknown signer");
  }

  @Test
  void invocationByte() {
    assertThat(SynapsKycbContract.Invocations.APPROVE).isEqualTo(1);
    assertThat(SynapsKycbContract.Invocations.KYC_REGISTER).isEqualTo(2);
    assertThat(SynapsKycbContract.Invocations.KYC_SUBMIT).isEqualTo(3);
  }

  @Test
  void callback_KycSubmit() {
    SynapsKycbContractState initial = withKycRegistration();
    Hash message = Hash.create(s -> s.write(KYC_REQUEST.getBytes(StandardCharsets.UTF_8)));
    Signature signature = synapsKeyPair.sign(message);
    SynapsKycbContractState state = submitKycInvocation(initial, KYC_REQUEST, signature);
    state = kycSubmitCallback(state, context.getFrom(), false);
    assertThat(state.getKycRegistration(context.getFrom())).isNotNull();
    state = kycSubmitCallback(state, context.getFrom(), true);
    assertThat(state.getKycRegistration(context.getFrom())).isNull();
  }

  private SynapsKycbContractState submitKybInvocation(
      SynapsKycbContractState state, String request, Signature signature) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(SynapsKycbContract.Invocations.APPROVE);
          rpc.writeString(request);
          signature.write(rpc);
          rpc.writeString(producerWebsite);
          rpc.writeInt(producerServerJurisdiction);
          producerPublicKey.write(rpc);
          producerBlsKey.write(rpc);
          popSignature.write(rpc);
        });
  }

  private SynapsKycbContractState submitKycInvocation(
      SynapsKycbContractState state, String request, Signature signature) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(SynapsKycbContract.Invocations.KYC_SUBMIT);
          rpc.writeString(request);
          signature.write(rpc);
          producerPublicKey.write(rpc);
          producerBlsKey.write(rpc);
          popSignature.write(rpc);
        });
  }

  private SynapsKycbContractState kycSubmitCallback(
      SynapsKycbContractState state, BlockchainAddress node, boolean success) {
    CallbackContext callbackContext = createCallbackContext(success);
    return serialization.callback(
        context, callbackContext, state, SynapsKycbContract.kycSubmitCallback(node));
  }

  private void verifyBpoInteraction(String request, List<ContractEvent> interactions) {
    assertThat(interactions).hasSize(1);
    assertThat(interactions.get(0)).isInstanceOf(ContractEventInteraction.class);
    ContractEventInteraction event = (ContractEventInteraction) interactions.get(0);
    byte[] actualRpc = SafeDataOutputStream.serialize(event.rpc);
    KycbRequest kycbRequest = new KycbRequest(request);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            BpOrchestrationRpc.addConfirmedBp(
                kycbRequest,
                producerWebsite,
                producerPublicKey,
                producerBlsKey,
                producerServerJurisdiction,
                popSignature));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  private static Signature convertFromEthereumSignature(String ethSignature) {
    int n = ethSignature.length();
    String recoveryId = ethSignature.substring(n - 2, n);
    return Signature.fromString(recoveryId + ethSignature.substring(0, n - 2));
  }

  private CallbackContext.ExecutionResult createExecutionResult(boolean success) {
    return CallbackContext.createResult(
        Hash.create(h -> h.writeString("EVENT_TRANSACTION")),
        success,
        SafeDataInputStream.createFromBytes(new byte[0]));
  }

  private CallbackContext createCallbackContext(boolean success) {
    CallbackContext.ExecutionResult executionResult = createExecutionResult(success);
    return CallbackContext.create(FixedList.create(List.of(executionResult)));
  }
}
