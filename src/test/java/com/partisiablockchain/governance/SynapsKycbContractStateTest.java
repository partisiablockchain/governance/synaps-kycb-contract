package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SynapsKycbContractStateTest {

  @Test
  void initial() {
    SynapsKycbContractState initial =
        SynapsKycbContractState.initial(
            SynapsKycbContractTest.bpOrchestrationContract, SynapsKycbContractTest.synapsPublicKey);
    Assertions.assertThat(initial.getBpOrchestrationContract())
        .isEqualTo(SynapsKycbContractTest.bpOrchestrationContract);
    Assertions.assertThat(initial.getVerificationKey())
        .isEqualTo(SynapsKycbContractTest.synapsPublicKey);
  }

  @Test
  void parseRequest() {
    Map<String, String> data =
        KycbRequest.parseStringAsJson(
            """
            {
              "corporate_id": 1,
              "name": "Corporation Ltd",
              "city": "Metropolis",
              "country": "Antarctica",
              "country-numeric": "250",
              "address": "Main street 123",
              "registration_number": "123456789",
              "provider": "Synaps",
              "partisia_blockchain_address": "000000000000000000000000000000000000000001"
            }
            """);
    Assertions.assertThat(data.get("corporate_id")).isEqualTo("1");
    Assertions.assertThat(data.get("name")).isEqualTo("Corporation Ltd");
    Assertions.assertThat(data.get("city")).isEqualTo("Metropolis");
    Assertions.assertThat(data.get("country")).isEqualTo("Antarctica");
    Assertions.assertThat(data.get("country-numeric")).isEqualTo("250");
    Assertions.assertThat(data.get("address")).isEqualTo("Main street 123");
    Assertions.assertThat(data.get("registration_number")).isEqualTo("123456789");
    Assertions.assertThat(data.get("provider")).isEqualTo("Synaps");
    Assertions.assertThat(data.get("partisia_blockchain_address"))
        .isEqualTo("000000000000000000000000000000000000000001");
  }
}
