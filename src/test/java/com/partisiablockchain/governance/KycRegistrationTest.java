package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

final class KycRegistrationTest {

  @Test
  void validate() {
    String name = "name";
    String address = "address";
    String city = "city";
    int country = 12;

    KycbRequest request =
        new KycbRequest(
            SynapsKycbContractTest.RAW_REQUEST.formatted(
                "", name, city, country, address, "000070000000000000000000000000000000000001"));

    assertThat(new KycRegistration(name, address, city, country, 13, "website").validate(request))
        .isTrue();
    assertThat(
            new KycRegistration("mismatch", address, city, country, 13, "website")
                .validate(request))
        .isFalse();
    assertThat(
            new KycRegistration(name, "mismatch", city, country, 13, "website").validate(request))
        .isFalse();
    assertThat(
            new KycRegistration(name, address, "mismatch", country, 13, "website")
                .validate(request))
        .isFalse();
    assertThat(new KycRegistration(name, address, city, 44, 13, "website").validate(request))
        .isFalse();
  }
}
