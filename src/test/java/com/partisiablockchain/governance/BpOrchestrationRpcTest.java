package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

final class BpOrchestrationRpcTest {

  @Test
  void addConfirmedBp() {
    KycbRequest kycbRequest = new KycbRequest(SynapsKycbContractTest.KYB_REQUEST);
    String website = "myWebsite";
    BlockchainPublicKey publicKey = new KeyPair(BigInteger.valueOf(123)).getPublic();
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(444));
    BlsPublicKey producerBlsKey = blsKeyPair.getPublicKey();
    int serverJurisdiction = 12;
    BlsSignature popSignature = blsKeyPair.sign(Hash.create(s -> s.writeString("pop")));
    byte[] rpc =
        SafeDataOutputStream.serialize(
            BpOrchestrationRpc.addConfirmedBp(
                kycbRequest, website, publicKey, producerBlsKey, serverJurisdiction, popSignature));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(BpOrchestrationRpc.BPO_ADD_CONFIRMED_BP);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(kycbRequest.getBlockProducerAddress());
    assertThat(stream.readString()).isEqualTo(kycbRequest.getName());
    assertThat(stream.readString()).isEqualTo(website);
    assertThat(stream.readString())
        .isEqualTo(kycbRequest.getAddress() + ", " + kycbRequest.getCity());
    assertThat(BlockchainPublicKey.read(stream)).isEqualTo(publicKey);
    assertThat(BlsPublicKey.read(stream)).isEqualTo(producerBlsKey);
    assertThat(stream.readInt()).isEqualTo(kycbRequest.getCountryNumeric());
    assertThat(stream.readInt()).isEqualTo(serverJurisdiction);
    assertThat(BlsSignature.read(stream)).isEqualTo(popSignature);
  }
}
