package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KycbRequestTest {

  @Test
  void duplicateKeys() {
    throwsParseError("{\"x\":1, \"x\":2}");
  }

  @Test
  void invalidString() {
    throwsParseError("{\"x\":\"y}");
    throwsParseError("{x:1}");
  }

  @Test
  void stringWithQuotes() {
    Map<String, String> map = KycbRequest.parseStringAsJson("{\"x \\\"and\\\" y\":123}");
    Assertions.assertThat(map.get("x \\\"and\\\" y")).isEqualTo("123");
  }

  @Test
  void invalidStart() {
    throwsParseError("x");
    throwsParseError("\"");
    throwsParseError("\\\"");
  }

  private void throwsParseError(String malformed) {
    Assertions.assertThatThrownBy(() -> KycbRequest.parseStringAsJson(malformed))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid request string");
  }
}
