package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.EventCreator;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.DataStreamSerializable;
import java.nio.charset.StandardCharsets;

/** Smart contract for accepting and checking KYCB messages from Synaps. */
@AutoSysContract(SynapsKycbContractState.class)
public final class SynapsKycbContract {

  /**
   * Initialize the Synaps KYCB contract.
   *
   * @param bpOrchestrationContract the address of the BP orchestration contract
   * @param verificationKey the public key for verification of the signature on the KYC approval
   * @return an initialized state for the contract with the supplied parameters
   */
  @Init
  public SynapsKycbContractState create(
      BlockchainAddress bpOrchestrationContract, BlockchainPublicKey verificationKey) {
    return SynapsKycbContractState.initial(bpOrchestrationContract, verificationKey);
  }

  /**
   * Upgrades an existing contract to this version.
   *
   * @param oldState state accessor for the old contract
   * @return the migrated state for the contract
   */
  @Upgrade
  public SynapsKycbContractState upgrade(StateAccessor oldState) {
    return SynapsKycbContractState.migrateState(oldState);
  }

  /**
   * Parses a KYB approval that has been signed by Synaps.
   *
   * <p>APPROVE accepts the following data (in order):
   *
   * <ol>
   *   <li>A string of JSON data that was obtained from Synaps
   *   <li>A Signature on the JSON data, also obtained from Synaps
   *   <li>The producer's website
   *   <li>The server jurisdiction of the producer's server
   *   <li>The producer's network public key
   *   <li>The producer's BLS public key
   *   <li>A proof-of-possession (in the form of a BLS signature) for the BLS public key
   * </ol>
   *
   * <p>If the signature from Synaps is valid, then ADD_CONFIRMED_BP is invoked on
   * bp-orchestration-contract.
   *
   * @param context the initial context of the contract
   * @param state the current state of the contract
   * @param jsonString JSON data that was obtained from Synaps
   * @param synapsSignature signature on the JSON data also obtained from Synaps
   * @param website the block producer's website
   * @param serverJurisdiction the server jurisdiction of the block producer's server
   * @param producerPublicKey the block producer's network public key
   * @param producerBlsKey the block producer's BLS public key
   * @param popSignature a proof-of-possession for the BLS public key
   * @return The same contract state
   */
  @Action(Invocations.APPROVE)
  public SynapsKycbContractState approve(
      SysContractContext context,
      SynapsKycbContractState state,
      String jsonString,
      Signature synapsSignature,
      String website,
      int serverJurisdiction,
      BlockchainPublicKey producerPublicKey,
      BlsPublicKey producerBlsKey,
      BlsSignature popSignature) {
    KycbRequest request = readKycbRequest(context, state, jsonString, synapsSignature);
    ensure(request.isKyb(), "KYC registration submitted as KYB");
    sendBpToBpo(
        context.getInvocationCreator(),
        state,
        request,
        website,
        serverJurisdiction,
        producerPublicKey,
        producerBlsKey,
        popSignature);
    return state;
  }

  /**
   * Register a KYC request that is to be validated by Synaps.
   *
   * <p>KYC_REGISTER accepts the following data (in order):
   *
   * <ol>
   *   <li>Name of individual
   *   <li>Address of individual
   *   <li>City of individual
   *   <li>Country of individual (ISO_3166-1_numeric)
   *   <li>The producer's website
   *   <li>The server jurisdiction of the producer's server (ISO_3166-1_numeric)
   * </ol>
   *
   * @param context the initial context of the contract
   * @param state the current state of the contract
   * @param name the name of the individual
   * @param address the address of the individual
   * @param city the city of the individual
   * @param country the country of the individual (ISO_3166-1_numeric)
   * @param website the website of the block producer
   * @param serverJurisdiction the server jurisdiction of the producer's server (ISO_3166-1_numeric)
   * @return A new state corresponding to the old state but with the specified registration added
   */
  @Action(Invocations.KYC_REGISTER)
  public SynapsKycbContractState kycRegister(
      SysContractContext context,
      SynapsKycbContractState state,
      String name,
      String address,
      String city,
      int country,
      String website,
      int serverJurisdiction) {

    return state.addKycRegistration(
        context.getFrom(),
        new KycRegistration(name, address, city, country, serverJurisdiction, website));
  }

  /**
   * Parses a KYC approval that has been signed by Synaps.
   *
   * <p>KYC_SUBMIT accepts the following data (in order):
   *
   * <ol>
   *   <li>A string of JSON data that was obtained from Synaps
   *   <li>A Signature on the JSON data, also obtained from Synaps
   *   <li>The producer's network public key
   *   <li>The producer's BLS public key
   *   <li>A proof-of-possession (in the form of a BLS signature) for the BLS public key
   * </ol>
   *
   * <p>If the signature from Synaps is valid and the data matches what has previously been
   * submitted to KYC_REGISTER, then ADD_CONFIRMED_BP is invoked on bp-orchestration-contract.
   *
   * @param context the initial context of the contract
   * @param state the current state of the contract
   * @param jsonString JSON data obtained from Synaps
   * @param synapsSignature signature on the JSON data, also obtained from Synaps
   * @param producerPublicKey the block producer's network public key
   * @param producerBlsKey the block producer's BLS public key
   * @param popSignature a proof-of-possession for the BLS public key
   * @return A new state corresponding to the old state but with the specified registration removed
   */
  @Action(Invocations.KYC_SUBMIT)
  public SynapsKycbContractState kycSubmit(
      SysContractContext context,
      SynapsKycbContractState state,
      String jsonString,
      Signature synapsSignature,
      BlockchainPublicKey producerPublicKey,
      BlsPublicKey producerBlsKey,
      BlsSignature popSignature) {
    KycbRequest request = readKycbRequest(context, state, jsonString, synapsSignature);
    ensure(!request.isKyb(), "KYB registration submitted as KYC");

    KycRegistration kycRegistration = state.getKycRegistration(context.getFrom());
    ensure(
        kycRegistration.validate(request),
        "Information in KYC registration must match information from Synaps");
    String website = kycRegistration.website();
    int serverJurisdiction = kycRegistration.serverJurisdiction();
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    sendBpToBpo(
        eventManager,
        state,
        request,
        website,
        serverJurisdiction,
        producerPublicKey,
        producerBlsKey,
        popSignature);
    eventManager.registerCallbackWithCostFromRemaining(kycSubmitCallback(context.getFrom()));
    return state;
  }

  static final class Invocations {
    static final int APPROVE = 1;
    static final int KYC_REGISTER = 2;
    static final int KYC_SUBMIT = 3;

    private Invocations() {}
  }

  static final class Callbacks {
    static final int KYC_SUBMIT_CALLBACK = 1;

    private Callbacks() {}
  }

  /**
   * Callback for submitting KYC approval.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext callback context
   * @param node the node that submitted the KYC request
   * @return state updated with the node removed from KYC registrations if the node was successfully
   *     added on the bpo contract.
   */
  @Callback(Callbacks.KYC_SUBMIT_CALLBACK)
  public SynapsKycbContractState kycSubmitCallback(
      SysContractContext context,
      SynapsKycbContractState state,
      CallbackContext callbackContext,
      BlockchainAddress node) {
    if (callbackContext.isSuccess()) {
      return state.removeKycRegistration(node);
    } else {
      return state;
    }
  }

  static DataStreamSerializable kycSubmitCallback(BlockchainAddress node) {
    return rpc -> {
      rpc.writeByte(Callbacks.KYC_SUBMIT_CALLBACK);
      node.write(rpc);
    };
  }

  private static void sendBpToBpo(
      EventCreator creator,
      SynapsKycbContractState state,
      KycbRequest request,
      String website,
      int serverJurisdiction,
      BlockchainPublicKey producerPublicKey,
      BlsPublicKey producerBlsKey,
      BlsSignature popSignature) {
    creator
        .invoke(state.getBpOrchestrationContract())
        .withPayload(
            BpOrchestrationRpc.addConfirmedBp(
                request,
                website,
                producerPublicKey,
                producerBlsKey,
                serverJurisdiction,
                popSignature))
        .sendFromContract();
  }

  private static KycbRequest readKycbRequest(
      SysContractContext context,
      SynapsKycbContractState state,
      String jsonString,
      Signature synapsSignature) {
    KycbRequest request = parseRequest(state.getVerificationKey(), jsonString, synapsSignature);
    ensure(
        context.getFrom().equals(request.getBlockProducerAddress()),
        "Sender must be block producer noted in Synaps request.");
    return request;
  }

  private static KycbRequest parseRequest(
      BlockchainPublicKey verificationKey, String jsonString, Signature signature) {
    Hash message = Hash.create(s -> s.write(jsonString.getBytes(StandardCharsets.UTF_8)));
    ensure(signature.recoverPublicKey(message).equals(verificationKey), "Unknown signer");
    return new KycbRequest(jsonString);
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }
}
