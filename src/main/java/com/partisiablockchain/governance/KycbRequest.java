package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.HashMap;
import java.util.Map;

/** A KYCB request pertaining to a particular block producer. */
public record KycbRequest(Map<String, String> bpInformation) {

  KycbRequest(String jsonString) {
    this(parseStringAsJson(jsonString));
  }

  /**
   * Parse a request from a JSON string as sent by Synaps. The parsing is VERY simple and makes a
   * number of assumptions about the format of the data:
   *
   * <ul>
   *   <li>The input is well-formed JSON
   *   <li>Only strings and ints are allowed for values
   * </ul>
   *
   * @param jsonString the string
   * @return a Kycb request
   */
  static Map<String, String> parseStringAsJson(String jsonString) {

    // The requests we parse are reasonably small, so we can make two passes over the string to make
    // parsing simpler.
    String trimmed = removeWhitespace(jsonString);

    Map<String, String> data = new HashMap<>();
    int current = skipSingleChar(trimmed, 0, '{');
    while (current < trimmed.length()) {
      String name = parseString(trimmed, current);
      // skip name + opening and closing quotes
      current += name.length() + 2;
      current = skipSingleChar(trimmed, current, ':');
      current = parseValue(data, name, trimmed, current);
      if (trimmed.charAt(current) != '}') {
        current = skipSingleChar(trimmed, current, ',');
      } else {
        current = skipSingleChar(trimmed, current, '}');
      }
    }
    return data;
  }

  private static String removeWhitespace(String toTrim) {
    StringBuilder builder = new StringBuilder();
    boolean inString = false;
    int current = 0;
    while (current < toTrim.length()) {
      char c = toTrim.charAt(current);
      // we enter/exit a string only if the current char is " and not escaped
      if (c == '"' && current > 0 && toTrim.charAt(current - 1) != '\\') {
        inString = !inString;
      }
      if (inString || !Character.isWhitespace(c)) {
        builder.append(c);
      }
      current++;
    }
    return builder.toString();
  }

  private static RuntimeException parseError() {
    return new RuntimeException("Invalid request string");
  }

  private static int skipSingleChar(String string, int from, char c) {
    if (string.charAt(from) != c) {
      throw parseError();
    }
    return from + 1;
  }

  /**
   * Finds an JSON attribute name starting from some index.
   *
   * @param string the string
   * @param from the beginning index
   * @return a string.
   */
  private static String parseString(String string, int from) {
    if (string.charAt(from) != '"') {
      throw parseError();
    }
    int attrEnd = from + 1;
    while (attrEnd < string.length()) {
      // skip any escaped characters.
      if (string.charAt(attrEnd) == '\\') {
        attrEnd += 2;
      }
      if (string.charAt(attrEnd) == '"') {
        break;
      } else {
        attrEnd++;
      }
    }
    // bork if we reach the end of the string. The caller guarantees that the end is not a quote, so
    // we know the name is malformed at this point.
    if (attrEnd == string.length()) {
      throw parseError();
    }
    return string.substring(from + 1, attrEnd);
  }

  private static String parsePositiveInt(String string, int from) {
    StringBuilder value = new StringBuilder();
    char c = string.charAt(from);
    while (Character.isDigit(c)) {
      value.append(c);
      from++;
      c = string.charAt(from);
    }
    return value.toString();
  }

  private static int parseValue(
      Map<String, String> data, String name, String jsonString, int current) {

    if (data.containsKey(name)) {
      throw parseError();
    }

    if (jsonString.charAt(current) == '"') {
      String value = parseString(jsonString, current);
      data.put(name, value);
      // skip the just parsed value + opening and closing "
      return current + value.length() + 2;
    } else {
      String value = parsePositiveInt(jsonString, current);
      data.put(name, value);
      return current + value.length();
    }
  }

  BlockchainAddress getBlockProducerAddress() {
    return BlockchainAddress.fromString(bpInformation.get("partisia_blockchain_address"));
  }

  /**
   * Gets the name for this node operator request via lookup in json.
   *
   * @return the name as string.
   */
  public String getName() {
    return bpInformation.get("name");
  }

  /**
   * Gets the address for this node operator request via lookup in json.
   *
   * @return the address as string.
   */
  public String getAddress() {
    return bpInformation.get("address");
  }

  /**
   * Gets the city for this node operator request via lookup in json.
   *
   * @return the city as string.
   */
  public String getCity() {
    return bpInformation.get("city");
  }

  /**
   * Gets the country code for this node operator request via lookup in json. See also <a
   * href="https://en.wikipedia.org/wiki/ISO_3166-1_numeric">wikipedia</a>.
   *
   * @return the country code as numeric.
   */
  public int getCountryNumeric() {
    return Integer.parseInt(bpInformation.get("country_numeric"));
  }

  /**
   * Determine if this is KYB or KYC.
   *
   * @return true if KYB, false if KYC
   */
  public boolean isKyb() {
    return bpInformation.get("corporate_id") != null;
  }

  /**
   * Concatenates the address and the city for this node operator.
   *
   * @return the full address as string.
   */
  public String getFullAddress() {
    return getAddress() + ", " + getCity();
  }
}
