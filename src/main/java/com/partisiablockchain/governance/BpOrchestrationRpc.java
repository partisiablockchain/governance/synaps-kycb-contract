package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.BlsSignature;
import com.secata.stream.DataStreamSerializable;

final class BpOrchestrationRpc {
  static final byte BPO_ADD_CONFIRMED_BP = 10;

  @SuppressWarnings("unused")
  private BpOrchestrationRpc() {}

  static DataStreamSerializable addConfirmedBp(
      KycbRequest request,
      String website,
      BlockchainPublicKey producerPublicKey,
      BlsPublicKey producerBlsKey,
      int serverJurisdiction,
      BlsSignature popSignature) {
    return stream -> {
      stream.writeByte(BPO_ADD_CONFIRMED_BP);
      request.getBlockProducerAddress().write(stream);
      stream.writeString(request.getName());
      stream.writeString(website);
      stream.writeString(request.getFullAddress());
      producerPublicKey.write(stream);
      producerBlsKey.write(stream);
      stream.writeInt(request.getCountryNumeric());
      stream.writeInt(serverJurisdiction);
      popSignature.write(stream);
    };
  }
}
