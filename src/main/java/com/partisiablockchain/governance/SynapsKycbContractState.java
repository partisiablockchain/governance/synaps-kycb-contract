package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.function.Function;

/** Contract state for {@link SynapsKycbContract}. */
@Immutable
public final class SynapsKycbContractState implements StateSerializable {

  private final BlockchainAddress bpOrchestrationContract;
  private final BlockchainPublicKey verificationKey;
  private final AvlTree<BlockchainAddress, KycRegistration> kycRegistrations;

  @SuppressWarnings("unused")
  SynapsKycbContractState() {
    bpOrchestrationContract = null;
    verificationKey = null;
    kycRegistrations = null;
  }

  private SynapsKycbContractState(
      BlockchainAddress bpOrchestrationContract,
      BlockchainPublicKey verificationKey,
      AvlTree<BlockchainAddress, KycRegistration> kycRegistrations) {
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.verificationKey = verificationKey;
    this.kycRegistrations = kycRegistrations;
  }

  /**
   * Create a new synaps-kycb-contract state.
   *
   * @param bpOrchestrationContract PBC address of the bp-orchestration-contract
   * @param verificationKey the public key of Synaps, used for verifying request
   * @return fresh SynapsKycbContractState object.
   */
  public static SynapsKycbContractState initial(
      BlockchainAddress bpOrchestrationContract, BlockchainPublicKey verificationKey) {
    return new SynapsKycbContractState(bpOrchestrationContract, verificationKey, AvlTree.create());
  }

  static SynapsKycbContractState migrateState(StateAccessor oldState) {
    BlockchainAddress bpOrchestrationContract =
        oldState.get("bpOrchestrationContract").blockchainAddressValue();
    BlockchainPublicKey verificationKey =
        oldState.get("verificationKey").blockchainPublicKeyValue();
    AvlTree<BlockchainAddress, KycRegistration> kycRegistrations;
    if (oldState.hasField("kycRegistrations")) {
      kycRegistrations =
          toAvlTree(
              oldState.get("kycRegistrations"),
              BlockchainAddress.class,
              KycRegistration::createFromAccessor);
    } else {
      kycRegistrations = AvlTree.create();
    }
    return new SynapsKycbContractState(bpOrchestrationContract, verificationKey, kycRegistrations);
  }

  /**
   * Get the bp-orchestration-contract address.
   *
   * @return the bp-orchestration-contract address.
   */
  public BlockchainAddress getBpOrchestrationContract() {
    return bpOrchestrationContract;
  }

  /**
   * Get the Synaps verification key.
   *
   * @return the Synaps verification key.
   */
  public BlockchainPublicKey getVerificationKey() {
    return verificationKey;
  }

  KycRegistration getKycRegistration(BlockchainAddress address) {
    return kycRegistrations.getValue(address);
  }

  SynapsKycbContractState addKycRegistration(
      BlockchainAddress address, KycRegistration registration) {
    return new SynapsKycbContractState(
        bpOrchestrationContract, verificationKey, kycRegistrations.set(address, registration));
  }

  SynapsKycbContractState removeKycRegistration(BlockchainAddress from) {
    return new SynapsKycbContractState(
        bpOrchestrationContract, verificationKey, kycRegistrations.remove(from));
  }

  AvlTree<BlockchainAddress, KycRegistration> getKycRegistrations() {
    return kycRegistrations;
  }

  private static <@ImmutableTypeParameter K extends Comparable<K>, @ImmutableTypeParameter V>
      AvlTree<K, V> toAvlTree(
          StateAccessor accessor, Class<K> keyClass, Function<StateAccessor, V> valueCreator) {
    AvlTree<K, V> map = AvlTree.create();
    for (StateAccessorAvlLeafNode leaf : accessor.getTreeLeaves()) {
      K key = leaf.getKey().cast(keyClass);
      V value = valueCreator.apply(leaf.getValue());
      map = map.set(key, value);
    }
    return map;
  }
}
