package com.partisiablockchain.governance;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** A KYC registration submitted for Synaps to later verify. */
@Immutable
public final class KycRegistration implements StateSerializable {

  private final String name;
  private final String address;
  private final String city;
  private final int country;

  private final int serverJurisdiction;
  private final String website;

  @SuppressWarnings("unused")
  KycRegistration() {
    name = null;
    address = null;
    city = null;
    country = 0;
    serverJurisdiction = 0;
    website = null;
  }

  KycRegistration(
      String name,
      String address,
      String city,
      int country,
      int serverJurisdiction,
      String website) {
    this.name = name;
    this.address = address;
    this.city = city;
    this.country = country;
    this.serverJurisdiction = serverJurisdiction;
    this.website = website;
  }

  static KycRegistration createFromAccessor(StateAccessor accessor) {
    String name = accessor.get("name").stringValue();
    String address = accessor.get("address").stringValue();
    String city = accessor.get("city").stringValue();
    int country = accessor.get("country").intValue();
    int serverJurisdiction = accessor.get("serverJurisdiction").intValue();
    String website = accessor.get("website").stringValue();
    return new KycRegistration(name, address, city, country, serverJurisdiction, website);
  }

  boolean validate(KycbRequest request) {
    return request.getName().equals(name())
        && request.getAddress().equals(address())
        && request.getCity().equals(city())
        && request.getCountryNumeric() == country();
  }

  String name() {
    return name;
  }

  String address() {
    return address;
  }

  String city() {
    return city;
  }

  int country() {
    return country;
  }

  int serverJurisdiction() {
    return serverJurisdiction;
  }

  String website() {
    return website;
  }
}
